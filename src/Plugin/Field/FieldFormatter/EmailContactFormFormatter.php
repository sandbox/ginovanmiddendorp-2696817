<?php

/**
 * @file
 * Contains \Drupal\link\Plugin\Field\FieldFormatter\EmailContactFormFormatter.
 */

namespace Drupal\email_contact_form_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormBuilder;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormState;

/**
 * Plugin implementation of the 'EmailContactFormFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "email_contact_form",
 *   label = @Translation("Email Contact Form"),
 *   field_types = {
 *     "email"
 *   }
 * )
 */
class EmailContactFormFormatter extends FormatterBase {

    /**
     * {@inheritdoc}
     */
    public static function defaultSettings() {
        return array(
            'contact_form' => '',
            'redirect_url' => '',
        ) + parent::defaultSettings();
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state) {

        $options = array();
        $contact_bundles  = entity_get_bundles('contact_message');
        foreach ($contact_bundles as $bundle => $bundle_info) {
            $options[$bundle] = $bundle_info['label'];
        }

        $form['contact_form'] = array(
            '#type' => 'select',
            '#title' => t('Contact form'),
            '#options' => $options,
            '#default_value' => $this->getSetting('contact_form'),
        );

        $form['redirect_url'] = array(
            '#type' => 'textfield',
            '#title' => t('Redirect url'),
            '#default_value' => $this->getSetting('redirect_url'),
        );

        $form['token_tree'] = array(
            '#theme' => 'token_tree',
            '#token_types' => array('node'),
            '#dialog' => TRUE,
        );

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode) {
        $element = [];
        foreach ($items as $item) {
            $to_address = $item->value;
            $contact_form_bundle = $this->getSetting('contact_form');

            // Get the email contact form.
            $message = \Drupal::entityTypeManager()->getStorage('contact_message')->create(array(
                'contact_form' => $contact_form_bundle,
            ));

            $message->getContactForm()->setRecipients([$to_address]);

            $form_object = \Drupal::entityTypeManager()->getFormObject($message->getEntityTypeId(), 'default');
            $form_object->setEntity($message);

            $form_state = (new FormState())->setFormState([]);

            $path = $this->getSetting('redirect_url');



            $redirect_url = Url::fromUserInput($path);

            $form_state->setTemporaryValue('build_by', 'email_contact_form_formatter');
            $form_state->setTemporaryValue('redirect_url', $redirect_url);

            $form = \Drupal::getContainer()->get('form_builder')->buildForm($form_object, $form_state);

            $form['#cache']['contexts'][] = 'user.permissions';
            $element[] = $form;
        }

        return $element;
    }

}
